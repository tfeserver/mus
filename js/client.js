"use strict";

class Client
{
    construct()
    {
        this.params= {};
    }

    async send(msg){
        console.log('sending to server: ', msg, JSON.stringify(msg));
        return this.socket.send(JSON.stringify(msg));
    }


    async connect(_params){
        var self=this;

        this.params = _params;
        this.params.ux.client = this;

        return new Promise(function(ok, reject)
        {
            try
            {
		    console.log('connect to',self.params);
                self.socket = new WebSocket((self.params.ssl ? "wss" : "ws")+"://"+self.params.host+"/",'tfe-game_v0');
                self.socket.onerror = (event) =>
                {
                    self.handleMessage({ command: 'socket_error', message:'Error Connecting to server' });
                    ok(false);
                };
                self.socket.onopen = (event) =>
                {
                    ok(true);
                };

                self.socket.onmessage = (event) =>
                {
                    try
                    {
                        var msg = JSON.parse(event.data);
                        self.handleMessage(JSON.parse(event.data));
                    }
                    catch(err)
                    {
                    }
                };

                self.socket.onclose = (event) =>
                {
                    self.handleMessage({ command: 'close', message: 'Socket closed' });
                };
            }
            catch(err)
            {
                console.log('connect catch error fail',err);
                reject('fail');
            }
        });
    }
    async handleMessage(msg)
    {
        if(Array.isArray(msg))
        {
            msg.forEach(submsg => this.handleMessage(submsg));
            return  true;
        }

        if(this.params.ux[msg.command])
        {
            this.params.ux[msg.command](msg);
        }
        else
        {
            console.error('Do not know what to do: ',msg);
        }
    }

}

