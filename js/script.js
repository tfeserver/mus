(function()
{
    "use strict";
    let game = new Game();
    window.game = game;

    document.addEventListener('DOMContentLoaded', game.init.bind(game, document.body));
})();
